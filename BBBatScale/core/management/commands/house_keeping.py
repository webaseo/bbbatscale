# encoding: utf-8
import logging
from datetime import timedelta
from django.core.management.base import BaseCommand
from django.utils.timezone import now
from core.models import Room
from core.services import reset_room
from core.utils import BigBlueButton
from django.db import transaction

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'perform health check on all servers'

    @transaction.non_atomic_requests
    def handle(self, *args, **options):
        logger.info("Start housekeeping")
        # clear monitoring values on inactive rooms
        for room in Room.objects.filter(last_running__lte=now() - timedelta(seconds=120)):
            if not BigBlueButton.validate_is_meeting_running(
                    BigBlueButton(room.server.dns, room.server.shared_secret).is_meeting_running(room.meeting_id)):
                with transaction.atomic():
                    if room.is_breakout or room.is_moodleroom():
                        room.delete()
                    else:
                        reset_room(room.meeting_id, room.name, room.config)
        logger.info("End housekeeping")
