# encoding: utf-8
import logging
from datetime import timedelta
from django.core.management.base import BaseCommand
from django.utils.timezone import now
from tendo.singleton import SingleInstance, SingleInstanceException
from core.constants import ROOM_STATE_CREATING
from core.models import Room, Server, GeneralParameter
from core.services import reset_room
from django.db import transaction

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'perform health check on all servers'

    @transaction.non_atomic_requests
    def handle(self, *args, **options):

        try:
            SingleInstance()
            # perform health check and data collection on all known server
            logger.info("Start checking servers stats")
            for server in Server.objects.all():
                server.collect_stats()
            logger.info("Done checking servers stats")

            logger.info("Start updating max counter.")
            # update max counter
            gp = GeneralParameter.load()
            participant_current = Room.get_participants_current()
            if participant_current > gp.participant_total_max:
                gp.participant_total_max = participant_current
                gp.save()
            logger.info("Done updating max counter.")
            for room in Room.objects.filter(last_running__lte=now() - timedelta(seconds=60),
                                            state=ROOM_STATE_CREATING):
                reset_room(room.meeting_id, room.name, room.config)

        except SingleInstanceException as e:
            logger.error(e)
