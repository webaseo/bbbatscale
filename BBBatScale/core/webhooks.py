import binascii
from datetime import datetime
import django_rq
import hmac
import json
import random
import requests
from rq import Retry

from .models import Webhook


# Webhook_compute_mac_header takes a Keyed-Hash Message Authentication Code
# (HMAC-SHA512) over the provided timestamp and body.  The result is suitably
# encoded for use as a value for the X-Hook-Signature header.
def webhook_compute_mac_header(key: bytes, timestamp: int, body: bytes) -> str:
    msg = b'%s.%s' % (str(timestamp).encode('utf-8'), body)
    mac = hmac.digest(key, msg, 'sha512')

    return 't=%d,v1=%s' % (timestamp, mac.hex())


def webhook_run(hook: Webhook, body: bytes) -> str:
    mac_timestamp = int(datetime.now().timestamp())
    sig = webhook_compute_mac_header(
        binascii.unhexlify(hook.secret), mac_timestamp, body)

    headers = {'Content-Type': 'application/json', 'X-Hook-Signature': sig}
    resp = requests.post(
        hook.url, headers=headers, data=body, timeout=hook.timeout.seconds)

    # ensure status code is in [200, 300)
    resp.raise_for_status()

    return 'hook %s: %d' % (hook, resp.status_code)


# Retry_intervals generates backoff intervals using the Full Jitter algorithm
# described (and performance-evaluated) at
# https://aws.amazon.com/blogs/architecture/exponential-backoff-and-jitter/
def retry_intervals(base, cap, n):
    for i in range(0, n):
        yield random.uniform(0, min(cap, (1 << i) * base))


def webhook_enqueue(event: str, payload: dict, **kwargs):
    BASE_RETRY_DELAY_SECONDS = 2
    MAX_RETRY_DELAY_SECONDS = 15

    timestamp = int(datetime.now().timestamp())

    hooks = Webhook.objects.filter(event=event, enabled=True)
    if not hooks.exists():
        return

    body = {
        'event': event,
        'ts': timestamp,
        'payload': payload,
    }
    encoded_body = json.dumps(
        body, separators=(',', ':'), sort_keys=True).encode('utf-8')

    q = django_rq.get_queue('webhooks', **(kwargs.get('queue_args') or {}))

    for hook in hooks:
        retry_policy = Retry(
            max=hook.num_retries,
            interval=list(
                retry_intervals(BASE_RETRY_DELAY_SECONDS,
                                MAX_RETRY_DELAY_SECONDS, hook.num_retries)),
        )

        q.enqueue(
            webhook_run,
            hook,
            encoded_body,
            retry=retry_policy,
        )
