# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-08-16 01:16+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

# TODO
#: support_chat/static/chat/js/chat.js:500
msgid "Support offline"
msgstr ""

# TODO
#: support_chat/static/chat/js/chat.js:503
msgid "Support online"
msgstr ""

# TODO
#: support_chat/static/chat/js/chat.js:506
msgid "Supporter joined"
msgstr ""

# TODO
#: support_chat/static/chat/js/chat.js:613
msgid "Support"
msgstr ""

# TODO
#: support_chat/static/chat/js/chat.js:642
msgid "Leave Chat"
msgstr ""

# TODO
#: support_chat/static/chat/js/chat.js:708
msgid "Message..."
msgstr ""

# TODO
#: support_chat/static/chat/js/chat.js:718
msgid "Send"
msgstr ""

# TODO
#: support_chat/static/chat/js/chat.js:728
msgid "Join Chat"
msgstr ""

# TODO
#: support_chat/static/chat/js/support.js:566
msgid "Inactivate Support"
msgstr ""

# TODO
#: support_chat/static/chat/js/support.js:569
msgid "Activate Support"
msgstr ""

# TODO
#: support_chat/static/chat/js/support.js:666
#: support_chat/static/chat/js/support.js:667
msgid "Search"
msgstr ""

# TODO
#: support_chat/static/chat/js/utilities.js:78
#, javascript-format
msgid "%s Message"
msgid_plural "%s Messages"
msgstr[0] ""
msgstr[1] ""
