from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponse
from django.shortcuts import render

from support_chat.utils import has_supporter_privileges


@login_required
@user_passes_test(has_supporter_privileges)
def support_chat_staff_overview(request) -> HttpResponse:
    return render(request, "support_chat/support_chat_staff_overview.html")
