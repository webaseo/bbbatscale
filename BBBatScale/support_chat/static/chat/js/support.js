/**
 * Create a new support container and initialize the connection to the server.
 *
 * @class
 * @requires Chat
 * @requires createWebSocket
 * @requires addListener
 * @requires updateUnreadMessagesBadge
 * @requires SortedList
 * @requires TabView
 * @param {string} basePath The base path to connect the WebSockets to (excluding the 'support' path element).
 * See [createWebSocket]{@link createWebSocket} for more information.
 * @param {?string} [logoURL=null] An url to a logo to be displayed by notifications as
 * [icon]{@link Notification.icon} or null if the notifications should not show an icon.
 * @return {Support}
 */
function Support(basePath, logoURL = null) {
    if (!(this instanceof Support)) {
        return new Support(basePath, logoURL);
    }

    /**
     * @type {Support}
     */
    const selfSupport = this;

    /**
     * @type {HTMLDivElement}
     */
    let container;
    /**
     * @type {HTMLDivElement}
     */
    let chatOverview;
    /**
     * @type {HTMLInputElement}
     */
    let chatNameFilter;
    /**
     * Container for chat elements without at least one unread messages.
     *
     * @type {HTMLDivElement}
     */
    let unreadChatOverviewList;
    /**
     * Container for chat elements without unread messages.
     *
     * @type {HTMLDivElement}
     */
    let readChatOverviewList;
    /**
     * @type {HTMLDivElement}
     */
    let chatTabContainer;

    /**
     * @type {HTMLButtonElement}
     */
    let toggleStatusButton;

    /**
     * @type {'active'|'inactive'}
     */
    let status = 'inactive';

    /**
     * @type {TabView}
     */
    let tabView;

    /**
     * @param {ChatEntry} lhs
     * @param {Date|ChatEntry} rhs
     * @return {number}
     */
    const chatEntryComparator = function (lhs, rhs) {
        let rhsTimestamp;
        if (!(rhs instanceof Date)) {
            rhsTimestamp = rhs.getLatestTimestamp();
            if (rhsTimestamp === null) {
                rhsTimestamp = Number.MAX_VALUE;
            } else {
                rhsTimestamp = rhsTimestamp.getTime();
            }
        } else {
            rhsTimestamp = rhs.getTime();
        }

        let lhsTimestamp = lhs.getLatestTimestamp();
        if (lhsTimestamp === null) {
            lhsTimestamp = Number.MAX_VALUE;
        } else {
            lhsTimestamp = lhsTimestamp.getTime();
        }

        return rhsTimestamp - lhsTimestamp;
    }

    /**
     * @param {ChatEntry} lhs
     * @param {ChatEntry} rhs
     * @return {boolean}
     */
    const chatEntryEqual = function (lhs, rhs) {
        return lhs.getChatOwner() === rhs.getChatOwner();
    }

    /**
     * @type {SortedList<string, ChatEntry>}
     */
    const chats = new SortedList(
        /**
         * @param {ChatEntry} lhs
         * @param {string|ChatEntry} rhs
         * @return {number}
         */
        function (lhs, rhs) {
            let rhsUsername;
            if (typeof rhs !== 'string') {
                rhsUsername = rhs.getChatOwner();
            } else {
                rhsUsername = rhs;
            }

            return rhsUsername.localeCompare(lhs.getChatOwner());
        }, chatEntryEqual);

    /**
     * @type {SortedList<Date, ChatEntry>}
     */
    const unreadChats = new SortedList(chatEntryComparator, chatEntryEqual);
    /**
     * @type {SortedList<Date, ChatEntry>}
     */
    const readChats = new SortedList(chatEntryComparator, chatEntryEqual);

    /**
     * @type {?WebSocket}
     */
    let webSocket = null;

    /**
     * @type {number}
     */
    let unreadMessagesCount = 0;

    /**
     * @type {?ChatEntry}
     */
    let activeTab = null;

    /**
     * Create a new overview entry.
     *
     * @class
     * @requires updateUnreadMessagesBadge
     * @requires formatDate
     * @param {string} chatOwner
     * @param {string} chatOwnerRealName
     * @return {ChatEntry}
     */
    function ChatEntry(chatOwner, chatOwnerRealName) {
        if (!(this instanceof ChatEntry)) {
            return new ChatEntry(chatOwner, chatOwnerRealName);
        }

        /**
         * @type {ChatEntry}
         */
        const selfChatEntry = this;

        /**
         * @type {Chat}
         */
        const chat = new Chat(basePath, {
            'chatOwner': chatOwner,
            'chatOwnerRealName': chatOwnerRealName,
            'support': selfSupport
        });

        /**
         * @type {HTMLDivElement}
         */
        let chatOverviewEntry;
        /**
         * @type {HTMLSpanElement}
         */
        let lastMessageTimestamp;
        /**
         * @type {HTMLSpanElement}
         */
        let unreadMessagesBadge;
        /**
         * @type {HTMLElement}
         */
        let lastMessage;

        /**
         * @type {?Date}
         */
        let latestTimestamp = null;

        /**
         * @type {?function(): Notification}
         */
        let notificationFactory = null;
        /**
         * @type {?number}
         */
        let delayedNotificationId = null;
        /**
         * @type {?Notification}
         */
        let notification = null;

        /**
         * Create the overview chatOverviewEntry.
         */
        {
            chatOverviewEntry = document.createElement('div');
            chatOverviewEntry.setAttribute('role', 'tab');
            chatOverviewEntry.classList.add('list-group-item', 'list-group-item-action');

            {
                const header = document.createElement('div');
                header.classList.add('d-flex', 'w-100', 'justify-content-between');
                chatOverviewEntry.appendChild(header);

                {
                    const title = document.createElement('h5');
                    title.classList.add('mb-1');
                    title.textContent = chatOwnerRealName;
                    header.appendChild(title);

                    const info = document.createElement('small');
                    header.appendChild(info);

                    {
                        lastMessageTimestamp = document.createElement('span');
                        info.appendChild(lastMessageTimestamp);

                        unreadMessagesBadge = document.createElement('span');
                        unreadMessagesBadge.classList.add('ml-1', 'badge', 'badge-secondary');
                        info.appendChild(unreadMessagesBadge);
                    }
                }

                lastMessage = document.createElement('small');
                lastMessage.classList.add('d-inline-block', 'text-truncate');
                lastMessage.style.maxWidth = '100%';
                chatOverviewEntry.appendChild(lastMessage);
            }

            const jQueryContainer = jQuery(chatOverviewEntry);
            jQueryContainer.on('click', function (event) {
                event.preventDefault();

                if (activeTab !== selfChatEntry) {
                    selfChatEntry.showTab();
                } else {
                    if (chatOverview.classList.contains('hidden')) {
                        chat.scrollDown();
                    } else {
                        selfSupport.hideChatOverview();
                    }
                    chat.focusMessageInput();
                }
            });
        }

        /**
         * Close the possibly displayed notification as well as an upcoming one, if there is one.
         */
        function closeNotification() {
            if (delayedNotificationId) {
                clearTimeout(delayedNotificationId);
            }
            if (notification) {
                notification.close();
                notification = null;
            }
        }

        /**
         * Displays this ChatEntry and hides the chat overview.
         */
        this.showTab = function () {
            if (activeTab !== this) {
                if (activeTab) {
                    activeTab.hideTab();
                }

                tabView.showTab(chat.getCard());
                chatOverviewEntry.classList.add('active');
                chat.open();
                jQuery(chat.getCard()).trigger('shown.supportchat.chat');

                activeTab = this;
            }
        };

        /**
         * Hides this ChatEntry and displays the chat overview.
         */
        this.hideTab = function () {
            if (activeTab === this) {
                tabView.hideTab();

                chatOverviewEntry.classList.remove('active');
                jQuery(chat.getCard()).trigger('hidden.supportchat.chat');
                chat.close();

                activeTab = null;
            }
        };

        /**
         * Update the unread messages badge and if the count is zero, closes the notification.
         *
         * @param {number} count
         */
        this.setUnreadMessagesCount = function (count) {
            updateUnreadMessagesBadge(jQuery(unreadMessagesBadge), count);

            if (count === 0) {
                moveChat(this, unreadChats, readChats, readChatOverviewList);

                closeNotification();
                notificationFactory = null;
            } else {
                moveChat(this, readChats, unreadChats, unreadChatOverviewList);

                function displayNotification() {
                    if (notificationFactory) {
                        closeNotification();
                        notification = notificationFactory();
                        notificationFactory = null;
                    }
                }

                if (chat.hasGainedSupport()) {
                    delayedNotificationId = setTimeout(displayNotification, 1000);
                } else {
                    displayNotification();
                }
            }
        };

        /**
         * Returns the username of the corresponding user.
         *
         * @return {string}
         */
        this.getChatOwner = function () {
            return chatOwner;
        };

        /**
         * Returns the real name of the corresponding user.
         *
         * @return {string}
         */
        this.getChatOwnerRealName = function () {
            return chatOwnerRealName;
        };

        /**
         * Return the chat entry html element.
         *
         * @return {HTMLDivElement}
         */
        this.getChatOverviewEntry = function () {
            return chatOverviewEntry;
        };

        /**
         * Update the message and timestamp previewed by the chatOverviewEntry and reorder the chat entries.
         *
         * @param {string} message
         * @param {Date} timestamp
         */
        this.updatePreview = function (message, timestamp) {
            lastMessage.textContent = message;
            lastMessageTimestamp.textContent = formatDate(timestamp);

            function updateTimestamp() {
                latestTimestamp = timestamp;
            }

            if (chat.getUnreadMessagesCount() === 0) {
                moveChat(this, readChats, readChats, readChatOverviewList, updateTimestamp);
            } else {
                moveChat(this, unreadChats, unreadChats, unreadChatOverviewList, updateTimestamp);
            }
        };

        /**
         * Update the message and timestamp previewed by the chatOverviewEntry,
         * reorder the chat entries, and create a Notification if the supporter is active.
         *
         * @param {string} message
         * @param {Date} timestamp
         */
        this.newMessage = function (message, timestamp) {
            this.updatePreview(message, timestamp);

            if (status === 'active' && 'Notification' in window && container.parentElement !== null) {
                // create only a factory to ensure the notification will not be shown,
                // if someone els already read the message.
                notificationFactory = function () {
                    const notificationOptions = {
                        'body': message,
                        'timestamp': timestamp.getTime()
                    };
                    if (logoURL) {
                        notificationOptions['icon'] = logoURL;
                    }

                    notification = new Notification(chatOwnerRealName, notificationOptions);
                    notification.addEventListener('click', function () {
                        selfChatEntry.showTab();
                        chat.scrollDown();
                        closeNotification();
                    });
                    notification.addEventListener('error', closeNotification);
                    notification.addEventListener('close', closeNotification);
                    return notification;
                }
            }
        };

        /**
         * @return {?Date} The timestamp previewed by the chat entry.
         */
        this.getLatestTimestamp = function () {
            return latestTimestamp;
        }

        /**
         * Changes the chatOverviewEntry to 'info' (green) in order to indicate
         * that at least one Supporter has entered the chat.
         */
        this.gainedSupport = function () {
            chatOverviewEntry.classList.add('list-group-item-info');
        };

        /**
         * Changes the chatOverviewEntry to its default (white) in order to indicate
         * that no Supporter is currently supporting the chat.
         */
        this.lostSupport = function () {
            chatOverviewEntry.classList.remove('list-group-item-info');
        };

        /**
         * Return the chat.
         *
         * @return {Chat}
         */
        this.getChat = function () {
            return chat;
        };

        /**
         * Update the unread messages count since all needed resources are now initialized.
         */
        {
            this.setUnreadMessagesCount(chat.getUnreadMessagesCount());
        }
    }

    /**
     * Sends the 'setStatus' command.
     * This will result in a callback to [onSupporterStatusChanged]{@link Support.onSupporterStatusChanged}.
     *
     * @param {boolean} active
     */
    function sendSetStatus(active) {
        webSocket.send(JSON.stringify({
            'type': 'setStatus',
            'status': (active ? 'active' : 'inactive')
        }));
    }

    /**
     * @param chatOwner
     * @return {?ChatEntry}
     */
    function getChatEntry(chatOwner) {
        const ownerChats = chats.find(chatOwner);
        if (ownerChats.length === 1) {
            return ownerChats[0];
        }
        console.assert(ownerChats.length === 0,
            'Illegal state: \'chats\' contains', ownerChats.length, 'entries of \'' + chatOwner + '\'');
        return null;
    }

    /**
     * @param {string} chatOwner
     * @param {string} chatOwnerRealName
     * @param {number} unreadMessages
     * @param {boolean} isSupportActive
     * @param {string} message
     * @param {Date} timestamp
     */
    function addChat(chatOwner, chatOwnerRealName, unreadMessages, isSupportActive, message, timestamp) {
        if (!chats.contains(chatOwner)) {
            const chatEntry = new ChatEntry(chatOwner, chatOwnerRealName);

            chats.add(chatEntry);
            chatEntry.updatePreview(message, timestamp);

            const chat = chatEntry.getChat();
            jQuery(chatTabContainer).append(chat.getCard());

            if (isSupportActive) {
                chatEntry.gainedSupport();
            } else {
                chatEntry.lostSupport();
            }
            chat.setUnreadMessagesCount(unreadMessages);
            updateUnreadMessagesCount();
        }
    }

    /**
     * Moves the supplied chatEntry from the fromList to the toList
     * and adding it at the corresponding position inside the overviewList.
     *
     * @param {ChatEntry} chatEntry
     * @param {SortedList<Date, ChatEntry>} fromList
     * @param {SortedList<Date, ChatEntry>} toList
     * @param {HTMLDivElement} overviewList
     * @param {?function(): void} afterRemove Called after the element has been removed.
     */
    function moveChat(chatEntry, fromList, toList, overviewList, afterRemove = null) {
        fromList.remove(chatEntry);
        if (afterRemove) {
            afterRemove();
        }

        const index = toList.ensureExists(chatEntry);
        if (index !== null) {
            const followingChatEntry = toList.get(index + 1);

            if (followingChatEntry) {
                jQuery(followingChatEntry.getChatOverviewEntry()).before(chatEntry.getChatOverviewEntry());
            } else {
                jQuery(overviewList).append(chatEntry.getChatOverviewEntry());
            }
        }
    }


    /**
     * Filters the chat entries by their user names with the filter
     * supplied by the [chatNameFilter]{@link chatNameFilter}.
     * If the filter, ignoring the case, is included in the user's name,
     * its entry will be shown, otherwise it will be hidden.
     */
    function filterChats() {
        const filter = chatNameFilter.value;

        chats.forEach(function (chatEntry) {
            if (!filter || chatEntry.getChatOwnerRealName().toLowerCase().includes(filter.toLowerCase())) {
                jQuery(chatEntry.getChatOverviewEntry()).show();
            } else {
                jQuery(chatEntry.getChatOverviewEntry()).hide();
            }
        });
    }

    /**
     * Updates the [toggleStatusButton]{@link toggleStatusButton} regarding to the current [status]{@link status}.
     */
    function updateToggleStatusButton() {
        if (status === 'active') {
            toggleStatusButton.classList.replace('btn-secondary', 'btn-success');
            toggleStatusButton.textContent = gettext('Inactivate Support');
        } else {
            toggleStatusButton.classList.replace('btn-success', 'btn-secondary');
            toggleStatusButton.textContent = gettext('Activate Support');
        }
    }

    /**
     * Update the message and timestamp previewed by the chatOverviewEntry and creates a Notification
     * if the supporter is active.
     *
     * @param {string} chatOwner
     * @param {string} message
     * @param {Date} timestamp
     */
    function newMessage(chatOwner, message, timestamp) {
        const chatEntry = getChatEntry(chatOwner);
        if (chatEntry) {
            chatEntry.newMessage(message, timestamp);
        }
    }

    /**
     * Updates the 'unread messages counter'.
     * If it has changed the [onUnreadMessagesCountUpdated]{@link Support.onUnreadMessagesCountUpdated} listener,
     * if set, will be called and the badges will be updated.
     */
    function updateUnreadMessagesCount() {
        let newUnreadMessagesCount = 0;
        chats.forEach(function (chatEntry) {
            const unreadMessagesCount = chatEntry.getChat().getUnreadMessagesCount();
            chatEntry.setUnreadMessagesCount(unreadMessagesCount);
            newUnreadMessagesCount += unreadMessagesCount;
        });
        if (unreadMessagesCount !== newUnreadMessagesCount) {
            unreadMessagesCount = newUnreadMessagesCount;

            if (selfSupport.onUnreadMessagesCountUpdated) {
                selfSupport.onUnreadMessagesCountUpdated(unreadMessagesCount);
            }
        }
    }

    /**
     * Changes the chatOverviewEntry to 'info' (green) in order to indicate
     * that at least one Supporter has entered the chat.
     *
     * @param {string} chatOwner
     */
    function chatGainedSupport(chatOwner) {
        const chatEntry = getChatEntry(chatOwner);
        if (chatEntry) {
            chatEntry.gainedSupport();
        }
    }

    /**
     * Changes the chatOverviewEntry to its default (white) in order to indicate
     * that no Supporter is currently supporting the chat.
     *
     * @param {string} chatOwner
     */
    function chatLostSupport(chatOwner) {
        const chatEntry = getChatEntry(chatOwner);
        if (chatEntry) {
            chatEntry.lostSupport();
        }
    }

    /**
     * Create the support container.
     */
    {
        container = document.createElement('div');
        container.classList.add('tab-view', 'mb-2');

        {
            chatOverview = document.createElement('div');
            chatOverview.classList.add('tab-overview', 'card', 'shadow-none', 'm-0');
            container.appendChild(chatOverview);

            {
                const cardHeader = document.createElement('div');
                cardHeader.classList.add('card-header', 'uniform');
                chatOverview.appendChild(cardHeader);

                {
                    const inputGroup = document.createElement('div');
                    inputGroup.classList.add('input-group');
                    cardHeader.appendChild(inputGroup);

                    {
                        chatNameFilter = document.createElement('input');
                        chatNameFilter.classList.add('form-control');
                        chatNameFilter.type = 'search';
                        chatNameFilter.placeholder = gettext('Search');
                        chatNameFilter.setAttribute('aria-label', gettext('Search'));
                        chatNameFilter.addEventListener('keyup', filterChats);
                        inputGroup.appendChild(chatNameFilter);

                        const inputGroupAppend = document.createElement('div');
                        inputGroupAppend.classList.add('input-group-append');
                        inputGroup.appendChild(inputGroupAppend);

                        {
                            const searchButton = document.createElement('button');
                            searchButton.classList.add('btn', 'btn-secondary');
                            searchButton.addEventListener('click', filterChats);
                            inputGroupAppend.appendChild(searchButton);

                            {
                                const searchButtonIcon = document.createElement('i');
                                searchButtonIcon.classList.add('fas', 'fa-search');
                                searchButton.appendChild(searchButtonIcon);
                            }
                        }
                    }
                }

                const cardBody = document.createElement('div');
                cardBody.classList.add('card-body', 'p-0');
                chatOverview.appendChild(cardBody);

                {
                    const chatOverviewList = document.createElement('div');
                    chatOverviewList.classList.add('mh-100', 'overflow-auto');
                    cardBody.appendChild(chatOverviewList);

                    {
                        unreadChatOverviewList = document.createElement('div');
                        unreadChatOverviewList.classList.add('list-group', 'list-group-flush', 'mh-100');
                        chatOverviewList.appendChild(unreadChatOverviewList);

                        readChatOverviewList = document.createElement('div');
                        readChatOverviewList.classList.add('list-group', 'list-group-flush', 'mh-100');
                        chatOverviewList.appendChild(readChatOverviewList);
                    }
                }
            }

            chatTabContainer = document.createElement('div');
            chatTabContainer.classList.add('tab-content');
            container.appendChild(chatTabContainer);
        }

        tabView = new TabView(container, chatOverview);
    }

    /**
     * Create toggle status button.
     */
    {
        toggleStatusButton = document.createElement('button');
        toggleStatusButton.classList.add('btn', 'btn-secondary');
        toggleStatusButton.addEventListener('click', function () {
            const active = status === 'active';
            sendSetStatus(!active);
            if (!active && 'Notification' in window && Notification.permission === 'default') {
                Notification.requestPermission().then();
            }
        });
        updateToggleStatusButton();
    }

    /**
     * Add event listeners.
     */
    {
        document.addEventListener('keydown', function (event) {
            if (event.key === 'Escape' && activeTab) {
                activeTab.getChat().leaveChat();
            }
        });
    }

    /**
     * Return the support container html element.
     *
     * @return {HTMLDivElement}
     */
    this.getContainer = function () {
        return container;
    };

    /**
     * Return the toggle status button html element.
     *
     * @return {HTMLButtonElement}
     */
    this.getToggleStatusButton = function () {
        return toggleStatusButton;
    };

    /**
     * Displays the chat overview.
     */
    this.showChatOverview = function () {
        tabView.showOverview();
    }

    /**
     * Hides the chat overview.
     */
    this.hideChatOverview = function () {
        tabView.hideOverview();
    }

    /**
     * Open the web socket for the chat.
     */
    this.open = function () {
        if (webSocket === null) {
            webSocket = createWebSocket(basePath, 'support');

            webSocket.onmessage = function (event) {
                const data = JSON.parse(event.data);
                if (!data.hasOwnProperty('type')) {
                    console.error('Received a malformed request:', JSON.stringify(data));
                    return;
                }
                const type = data['type'];
                switch (type) {
                    case 'error': {
                        console.error('An unexpected error occurred [code:', data['code'] + ']:', data['message']);
                        break;
                    }
                    case 'status': {
                        if (['active', 'inactive'].includes(data['status'])) {
                            status = data['status'];
                        } else {
                            console.error('Unrecognized supporter status \'' + data['status'] + '\'');
                            return;
                        }
                        updateToggleStatusButton();

                        if (selfSupport.onSupporterStatusChanged) {
                            selfSupport.onSupporterStatusChanged(status);
                        }
                        break;
                    }
                    case 'chat': {
                        addChat(data['chatOwner'], data['chatOwnerRealName'], data['unreadMessages'],
                            data['isSupportActive'], data['message'], new Date(data['timestamp']));
                        break;
                    }
                    case 'newMessage': {
                        newMessage(data['chatOwner'], data['message'], new Date(data['timestamp']));
                        break;
                    }
                    case 'unreadMessagesCount': {
                        const chatEntry = getChatEntry(data['chatOwner']);
                        if (chatEntry) {
                            chatEntry.getChat().setUnreadMessagesCount(data['unreadMessagesCount']);
                            updateUnreadMessagesCount();
                        }
                        break;
                    }
                    case 'chatGainedSupport': {
                        chatGainedSupport(data['chatOwner']);
                        break;
                    }
                    case 'chatLostSupport': {
                        chatLostSupport(data['chatOwner']);
                        break;
                    }
                    default: {
                        console.error('Received an unexpected type:', type);
                        break;
                    }
                }
            };

            webSocket.onopen = function () {
                webSocket.send(JSON.stringify({
                    'type': 'getStatus'
                }));
                webSocket.send(JSON.stringify({
                    'type': 'getChats'
                }));
            };

            webSocket.onclose = function (error) {
                if (error.code !== 1000 && error.code !== 1001) {
                    console.error('Chat socket closed unexpectedly: ' + error.code);
                }
            };
        }
    };

    /**
     * @return {boolean} True if the chat connection to the server is open and ready, otherwise false.
     */
    this.isOpen = function () {
        return webSocket !== null && webSocket.readyState === WebSocket.OPEN;
    };

    /**
     * @return {'active'|'inactive'} 'active' if the supporter has set his/her status to active, 'inactive' otherwise.
     */
    this.getStatus = function () {
        return status;
    };

    /**
     * Called when the supporter status changed.
     * The function will be called with one argument which is either the string 'active' or 'inactive'.
     *
     * @type {?function(status: string): void}
     */
    this.onSupporterStatusChanged = null;

    /**
     * Adds the listener to [onSupporterStatusChanged]{@link Support.onSupporterStatusChanged}
     * without removing the previous one, if any.
     *
     * @param {function(status: string): void} listener
     */
    this.addSupporterStatusChangedListener = function (listener) {
        addListener(this, 'onSupporterStatusChanged', listener);
    }

    /**
     * Called when the 'unread messages counter' has been updated.
     * The function will be called with one argument which is the total amount of unread messages.
     *
     * @type {?function(totalUnreadMessages: number): void}
     */
    this.onUnreadMessagesCountUpdated = null;

    /**
     * Adds the listener to [onUnreadMessagesCountUpdated]{@link Support.onUnreadMessagesCountUpdated}
     * without removing the previous one, if any.
     *
     * @param {function(totalUnreadMessages: number): void} listener
     */
    this.addUnreadMessagesCountUpdatedListener = function (listener) {
        addListener(this, 'onUnreadMessagesCountUpdated', listener);
    }

    /**
     * Closes the connection to the server.
     */
    this.close = function () {
        if (webSocket !== null) {
            webSocket.close(1000);
            webSocket = null;
        }
    };
}