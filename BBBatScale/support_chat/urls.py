from django.urls import path
from django.views.i18n import JavaScriptCatalog

from support_chat import views

urlpatterns = [
    path("jsi18n", JavaScriptCatalog.as_view(packages=["support_chat"]), name="support_chat_javascript_catalog"),
    path("overview", views.support_chat_staff_overview, name="support_chat_staff_overview"),
]
