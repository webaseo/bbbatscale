stages:
  - commit:build
  - commit:test
  - commit:analysis
  - commit:tag
  - commit:release

include:
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: SAST.gitlab-ci.yml
  - template: License-Scanning.gitlab-ci.yml

dev_build:
  stage: commit:build
  rules:
    - when: on_success
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/docker/common/Dockerfile --destination $CI_REGISTRY_IMAGE/dev-container:$CI_COMMIT_SHA

pytest:
  stage: commit:test
  rules:
    - when: on_success
  image:
    name: $CI_REGISTRY_IMAGE/dev-container:$CI_COMMIT_SHA
  variables:
    POSTGRES_DB: bbbatscale
    POSTGRES_USER: bbbatscale
    POSTGRES_PASSWORD: SuperSecretPipelinePassword
    POSTGRES_HOST: postgres
    POSTGRES_PORT: 5432
    REDIS_HOST: redis
    REDIS_PORT: 6379
    BASE_URL: localhost
    DJANGO_SECRET_KEY: SuperSecretPipelinePassword
    RECORDINGS_SECRET: SuperSecretPipelinePassword
    SUPPORT_CHAT: enabled
  services:
    - name: postgres:12.3-alpine
    - name: redis:6.0.5-alpine
  before_script:
    - await_postgres_connection "$POSTGRES_HOST" "$POSTGRES_DB" "$POSTGRES_USER" 10
  script:
    - pytest
      --junitxml=report.xml
      --cov-report=xml:coverage.xml --cov-config=coveragerc.ini --cov=BBBatScale
      BBBatScale
  artifacts:
    reports:
      junit: report.xml
    paths:
      - coverage.xml
  needs:
    - job: dev_build

flake8:
  stage: commit:test
  rules:
    - when: on_success
  image:
    name: $CI_REGISTRY_IMAGE/dev-container:$CI_COMMIT_SHA
    entrypoint: [""]
  script:
    - flake8 --config=flake8.ini --output-file=flake8Report.txt
  artifacts:
    paths:
      - flake8Report.txt
  needs:
    - job: dev_build

dependency_scanning:
  stage: commit:analysis
  allow_failure: true
  variables:
    DS_DISABLE_DIND: "true"
    DS_REMEDIATE: "false"
    DS_DEFAULT_ANALYZERS: "gemnasium-python"
  before_script:
    - apt -y install libpq-dev build-essential python3-dev libldap2-dev libsasl2-dev ldap-utils tox lcov valgrind
  needs: []

sast:
  stage: commit:analysis
  allow_failure: true
  variables:
    SAST_DISABLE_DIND: "true"
    SAST_DEFAULT_ANALYZERS: "bandit, secrets"
  needs: []

license_scanning:
  stage: commit:analysis
  needs: []

sonarqube:
  stage: commit:analysis
  rules:
    - if: $SONARQUBE_TOKEN == null || $CI_COMMIT_BRANCH == null
      when: never
    - when: on_success
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  script:
    - sonar-scanner
      -Dsonar.qualitygate.wait=true
      -Dsonar.projectKey=bbbatscale
      -Dsonar.sources=BBBatScale
      -Dsonar.host.url=https://bbbatscale.sonar.fbi.h-da.de
      -Dsonar.login=$SONARQUBE_TOKEN
      -Dsonar.python.coverage.reportPaths=coverage.xml
      -Dsonar.cpd.exclusions=**/settings/**
      -Dsonar.coverage.exclusions=**/migrations/**
      -Dsonar.exclusions=**/staticfiles/**,**/static/**
      -Dsonar.python.pylint.reportPath=flake8Report.txt
      -Dsonar.branch.name=$CI_COMMIT_BRANCH
  allow_failure: true # TODO remove after test coverage has been improved
  needs:
    - job: pytest
      artifacts: true
    - job: flake8
      artifacts: true

tag_container_image_master:
  stage: commit:tag
  rules:
    - if: "$CI_COMMIT_BRANCH == 'master'"
      when: on_success
    - when: never
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [""]
  script:
    - crane auth login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
    - crane cp "$CI_REGISTRY_IMAGE/dev-container:$CI_COMMIT_SHA" "$CI_REGISTRY_IMAGE:master"

create_new_version:
  image: node:13
  stage: commit:release
  rules:
    - if: "$CI_COMMIT_BRANCH == 'master'"
      when: manual
    - when: never
  script:
    - npm install @semantic-release/gitlab
    - npx semantic-release

tag_image_with_version:
  stage: commit:tag
  rules:
    - if: '$CI_COMMIT_TAG =~ /\d{1,3}\.\d{1,3}\.\d{1,3}(\d{1,3})?/'
      when: on_success
    - when: never
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [""]
  script:
    - crane auth login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
    - crane auth login -u "$DOCKERHUB_USER" -p "$DOCKERHUB_TOKEN" docker.io
    - crane cp "$CI_REGISTRY_IMAGE/dev-container:$CI_COMMIT_SHA" "$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG"
    - crane cp "$CI_REGISTRY_IMAGE/dev-container:$CI_COMMIT_SHA" "docker.io/$DOCKERHUB_REPO_PATH:$CI_COMMIT_TAG"
    - crane cp "$CI_REGISTRY_IMAGE/dev-container:$CI_COMMIT_SHA" "$CI_REGISTRY_IMAGE:latest"
